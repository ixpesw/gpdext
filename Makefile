# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

#Here we check the necessary environment variables
$(call check_defined, BOOST_ROOT,"Please set the BOOST_ROOT variable")
$(call check_defined, CFITSIO_ROOT,"Please set the CFITSIO_ROOT variable")
$(call check_defined, QT_ROOT,"Please set the QT_ROOT variable")
$(call check_defined, GEANT_ROOT,"Please set the GEANT_ROOT variable")

# Default behaviour of 'make' is to check if the package already exists:
# if it does, delete only the build/install directories and recompile;
# if not, it starts from scratch, i.e. by extracting the tarball.
# Each package xxx can be compiled separately with 'make xxx'
# (e.g 'make boost')
# There are three alternative commands for each package:
#   - 'make cleanxxx' will only delete the build/install directories and exit
#   - 'make erasexxx' will remove everything but the tarball and exit
#   - 'make remakexxx' will remove everything but the tarball and reinstall it
#     from scratch

all: boost cfitsio qt geant

erase: eraseboost erasecfitsio eraseqt erasegeant

remake: erase all

#
# Applications.
#
.PHONY: boost
boost:
	./install_boost.sh

cleanboost:
	./install_boost.sh --clean

eraseboost:
	./install_boost.sh --erase

remakeboost:
	./install_boost.sh --scratch


.PHONY: cfitsio
cfitsio:
	./install_cfitsio.sh

cleancfitsio:
	./install_cfitsio.sh --clean

erasecfitsio:
	./install_cfitsio.sh --erase

remakecfitsio:
	./install_cfitsio.sh --scratch


.PHONY: qt
qt:
	./install_qt.sh
	./install_qcustomplot.sh

cleanqt:
	./install_qcustomplot.sh --clean
	./install_qt.sh --clean

eraseqt:
	./install_qcustomplot.sh --erase
	./install_qt.sh --erase

remakeqt:
	./install_qt.sh --scratch
	./install_qcustomplot.sh --scratch


.PHONY: geant
geant:
	./install_geant.sh

cleangeant:
	./install_geant.sh --clean

erasegeant:
	./install_geant.sh --erase

remakegeant:
	./install_geant.sh --scratch

