#!/bin/bash

#Functions:

_clean()
{
    echo "Cleaning the build dir(s)..."
    [ -d "$BOOST_INSTALL_DIR" ] && rm -rf "$BOOST_INSTALL_DIR"
    [ -d "$BOOST_BUILD_DIR" ] && rm -rf "$BOOST_BUILD_DIR"
}

_erase()
{
    echo "Removing previously created directories (if any)..."    
    [ -d "$BOOST_ROOT" ] && rm -rf "$BOOST_ROOT"
}

extract()
{
  # If a directory with the target name already exists we remove it first
  _erase
  # Extract the compressed package
  echo "Extracting the tarball..."
  tar -xf "$BOOST_ROOT".tar.xz --directory "$GPDEXTROOT"
}

buildb2()
{
    # Build Boost.Build (a.k.a b2)
    mkdir -p "$B2_INSTALL_DIR"
    cd "$BOOST_ROOT"/tools/build
    # check that we actually moved
    if [ $? -ne 0 ]; then
      exit 1
    fi
    ./bootstrap.sh --with-toolset="$CC"
    ./b2 install toolset="$CC" --prefix="$B2_INSTALL_DIR"
}

##################

# Main

WORKDIR=$(pwd)

# Check against multiple argument
if [ $# -gt 1 ]; then
   echo "ERROR: Multiple flags 's', 'c', and 'e' not allowed."
   echo "Exiting..."
   exit 1
fi

# The following loop will read the command line options
# see http://linuxcommand.org/lc3_wss0120.php
scratch=
clean= 
erase=
while [ "$1" != "" ]; do
    case $1 in
        -s | --scratch )        shift
                                scratch=0
                                ;;
        -c | --clean )          shift
                                clean=0
                                ;;
        -e | --erase )          shift
                                erase=0
                                ;;
        * )                     exit 1
    esac
    shift
done

# Check if GPDEXTROOT exists (the syntax comes from here)
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${GPDEXTROOT+x} ]; then
    echo "GPDEXTROOT is unset"
    exit 1
fi


# clean option: remove the build directories
if [ -n "$clean" ]; then
    _clean
    exit 0
fi

# erase option: remove everything
if [ -n "$erase" ]; then
    _erase
    exit 0
fi

echo "Installing Boost libraries..."

#
# Set the building toolset:
# 'gcc' is the default, but we use 'darwin' on Mac OS systems
#
if [ $(uname) == "Darwin" ]; then
    CC=darwin
else
    CC=gcc
fi
echo "Building with" $CC


# If the scratch option is active or if a previous in installation does not
# already exist we need to call the 'extract' function first and then
# build Boost.Build (alias b2), which will then be used to build the rest
if [ -n "$scratch" ]; then
    echo "'scratch' option selected: completely removing previous installation..."
    extract
    buildb2
else
    if [ -d "$BOOST_ROOT" ]; then
        # In this case we do not build b2 nor extract the tarballs
        echo "Previous installation found. Rebuilding only..."
        # Remove only the build directories
        _clean
    else
        echo "No previous installation found. Starting from scratch..."
        extract
        buildb2
    fi
fi

# Use Boost.Build to build everything else
cd "$BOOST_ROOT"
# check that we actually moved
if [ $? -ne 0 ]; then
  exit 1
fi
"$B2_INSTALL_DIR"/bin/b2 toolset="$CC" --with-log --with-program_options --with-timer --with-math stage

#Leave the user in the intial directory
cd "$WORKDIR"
