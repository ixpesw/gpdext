#!/bin/bash

#Functions:

_clean()
{
    echo "Cleaning the build dir(s)..."
    [ -d "$CFITSIO_BUILD_DIR" ] && rm -rf "$CFITSIO_BUILD_DIR"
}

_erase()
{
    echo "Removing previously created directories (if any)..."
    [ -d "$CFITSIO_ROOT" ] && rm -rf "$CFITSIO_ROOT"
    [ -d "$CFITSIO_PATCH_DIR" ] && rm -rf "$CFITSIO_PATCH_DIR"
}

extract()
{
  # If a directory with the target name already exists we remove it first
  _erase
  # Extract the compressed package
  echo "Extracting the tarball..."
  tar -xf "$GPDEXTROOT"/"$CFITSIO_FULL_NAME".tar.gz --directory "$GPDEXTROOT"
  # Change the name of the extracted directory to reflect the version number
  mv "$GPDEXTROOT"/cfitsio "$CFITSIO_ROOT"
  # Here we extract and apply the patch to make things work on MacOS 10.14.6
  # see issue #19
  echo "Extracting the patch for MacOS 10.14.6 compatibility..."
  tar -xf "$GPDEXTROOT"/"$CFITSIO_PATCH_NAME".tar.gz --directory "$GPDEXTROOT"
  source "$CFITSIO_PATCH_DIR"/"$CFITSIO_PATCH_NAME".sh
  echo "Removing the patch extracted dir (not needed anymore)..."
  [ -d "$CFITSIO_PATCH_DIR" ] && rm -rf "$CFITSIO_PATCH_DIR"
}

##################

# Main

WORKDIR=$(pwd)

# Check against multiple argument
if [ $# -gt 1 ]; then
   echo "ERROR: Multiple flags 's', 'c', and 'e' not allowed."
   echo "Exiting..."
   exit 1
fi

# The following loop will read the command line options
# see http://linuxcommand.org/lc3_wss0120.php
scratch=
clean=
erase=
while [ "$1" != "" ]; do
    case $1 in
        -s | --scratch )        shift
                                scratch=0
                                ;;
        -c | --clean )          shift
                                clean=0
                                ;;
        -e | --erase )          shift
                                erase=0
                                ;;
        * )                     exit 1
    esac
    shift
done

# Check if GPDEXTROOT exists (the syntax comes from here)
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${GPDEXTROOT+x} ]; then
    echo "GPDEXTROOT is unset"
    exit 1
fi

# clean option: remove the build directories
if [ -n "$clean" ]; then
    _clean
    exit 0
fi

# erase option: remove everything
if [ -n "$erase" ]; then
    _erase
    exit 0
fi

echo "Installing cfitsio libraries..."

# If the scratch option is active or if a previous in installation does not
# already exists we need to call the 'extract' function first
if [ -n "$scratch" ]; then
    echo "'scratch' option selected: completely removing previous installation..."
    extract
else
    if [ -d "$CFITSIO_ROOT" ]; then
        echo "Previous installation found. Rebuilding only..."
        # Remove only the build dir
        _clean
    else
        echo "No previous installation found. Starting from scratch..."
        extract
    fi
fi

mkdir -p $CFITSIO_BUILD_DIR

cd "$CFITSIO_ROOT"
echo "Installing in: $CFITSIO_BUILD_DIR"
./configure --prefix=$CFITSIO_BUILD_DIR
echo "Start building..."
make shared
echo "Done."
echo "Installing..."
make install
echo "Cleaning source directory..."
make clean
echo "Done."

#Leave the user in the initial directory
cd "$WORKDIR"
