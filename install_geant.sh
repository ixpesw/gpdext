#!/bin/bash

#Functions:

_clean()
{
    echo "Cleaning the build dir(s)..."
    # The install dir is inside the build dir, so we need to remove only the latter
    [ -d "$GEANT_BUILD_DIR" ] && rm -rf "$GEANT_BUILD_DIR"
}

_erase()
{
    _clean
    echo "Removing previously created directories (if any)..."
    [ -d "$GEANT_ROOT" ] && rm -rf "$GEANT_ROOT"
    [ -d "$QT_PATCH_DIR" ] && rm -rf "$QT_PATCH_DIR"
}

extract()
{
  # If a directory with the target name already exists we remove it first
  _erase
  # Extract the compressed Geant4 package
  echo "Extracting the tarball..."
  # Make sure the output directory name is what we want
  # See https://unix.stackexchange.com/a/11019
  mkdir -p $GEANT_ROOT
  tar -xf "$GPDEXTROOT"/"$GEANT_FULL_NAME".tar.gz -C $GEANT_ROOT --strip-components 1
  # Extract the required data packages
  GEANT_DATA_DIR="$GEANT_ROOT"/data
  mkdir -p "$GEANT_DATA_DIR"
  tar -xf "$GPDEXTROOT"/G4EMLOW."$G4LEDATAVER".tar.gz --directory "$GEANT_DATA_DIR"
  tar -xf "$GPDEXTROOT"/G4ENSDFSTATE."$G4ENSDFSTATEVER".tar.gz --directory "$GEANT_DATA_DIR"
  # Here we extract and apply the patch to make things work on gcc11 and later
  # (see issue #25)
  echo "Extracting the patch for gcc11 compatibility..."
  tar -xf "$GPDEXTROOT"/"$GEANT_PATCH_NAME".tar.gz --directory "$GPDEXTROOT"
  source "$GEANT_PATCH_DIR"/"$GEANT_PATCH_NAME".sh
  echo "Removing the patch extracted dir (not needed anymore)..."
  [ -d "$GEANT_PATCH_DIR" ] && rm -rf "$GEANT_PATCH_DIR"
}

##################

# Main

WORKDIR=$(pwd)

# Check against multiple argument
if [ $# -gt 1 ]; then
   echo "ERROR: Multiple flags 's', 'c', and 'e' not allowed."
   echo "Exiting..."
   exit 1
fi

# The following loop will read the command line options
# see http://linuxcommand.org/lc3_wss0120.php
scratch=
clean=
erase=
while [ "$1" != "" ]; do
    case $1 in
        -s | --scratch )        shift
                                scratch=0
                                ;;
        -c | --clean )          shift
                                clean=0
                                ;;
        -e | --erase )          shift
                                erase=0
                                ;;
        * )                     exit 1
    esac
    shift
done

# Check if GPDEXTROOT exists (the syntax comes from here)
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${GPDEXTROOT+x} ]; then
    echo "GPDEXTROOT is unset"
    exit 1
fi


# clean option: remove the build directories
if [ -n "$clean" ]; then
    _clean
    exit 0
fi

# erase option: remove everything
if [ -n "$erase" ]; then
    _erase
    exit 0
fi

echo "Installing Geant4..."

# If the scratch option is active or if a previous in installation does not
# already exists we need to call the 'extract' function first
if [ -n "$scratch" ]; then
    echo "'scratch' option selected: completely removing previous installation..."
    extract
else
    if [ -d "$GEANT_ROOT" ]; then
        echo "Previous installation found. Rebuilding only..."
        # Remove only the build dir
        _clean
    else
        echo "No previous installation found. Starting from scratch..."
        extract
    fi
fi


BUILD_MULTITHREADED=OFF
BUILD_QT=OFF

mkdir -p "$GEANT_BUILD_DIR"

cd "$GEANT_BUILD_DIR"

mkdir -p "$GEANT_INSTALL_DIR"

cmake -DCMAKE_INSTALL_PREFIX="$GEANT_INSTALL_DIR" \
      -DGEANT4_BUILD_MULTITHREADED=$BUILD_MULTITHREADED \
      -DGEANT4_USE_QT=$BUILD_QT \
      -DGEANT4_INSTALL_DATADIR="$GEANT_DATA_DIR" \
      -DGEANT4_USE_OPENGL_X11=ON \
      "$GEANT_ROOT"

make -j"$N_CORES_USED"
make install

#Leave the user in the initial directory
cd "$WORKDIR"
