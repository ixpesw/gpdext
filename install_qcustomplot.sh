#!/bin/bash

#Functions:

_erase()
{
    echo "Removing previously created directories (if any)..."    
    [ -d "$QCUSTOMPLOT_ROOT" ] && rm -rf "$QCUSTOMPLOT_ROOT"
}

extract()
{
  # If a directory with the target name already exists we remove it first
  _erase
  # Extract the compressed package
  tar -xf "$GPDEXTROOT"/"$QCUSTOMPLOT_FULL_NAME".tar.gz --directory "$GPDEXTROOT"
}

_clean()
{
    # To be implemented yet...
    echo "WARNING: A proper '_clean()' function is not implemented yet for this application."
    echo "'_erase() and _extract()' will be used instead (with the same effect)."
    #echo "Cleaning the build dir(s)..."
    _erase
    extract
}

##################

# Main

WORKDIR=$(pwd)

# Check against multiple argument
if [ $# -gt 1 ]; then
   echo "ERROR: Multiple flags 's', 'c', and 'e' not allowed."
   echo "Exiting..."
   exit 1
fi

# The following loop will read the command line options
# see http://linuxcommand.org/lc3_wss0120.php
scratch=
clean= 
erase=
while [ "$1" != "" ]; do
    case $1 in
        -s | --scratch )        shift
                                scratch=0
                                ;;
        -c | --clean )          shift
                                clean=0
                                ;;
        -e | --erase )          shift
                                erase=0
                                ;;
        * )                     exit 1
    esac
    shift
done

# Check if GPDEXTROOT exists (the syntax comes from here)
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${GPDEXTROOT+x} ]; then
    echo "GPDEXTROOT is unset"
    exit 1
fi

# clean option: remove the build directories
if [ -n "$clean" ]; then
    echo "The 'clean' function is not implemented yet for this application."
    #_clean
    _erase
    exit 0
fi

# erase option: remove everything
if [ -n "$erase" ]; then
    _erase
    exit 0
fi

echo "Compiling QCustomPlot shared libraries..."

# If the scratch option is active or if a previous in installation does not
# already exist we need to call the 'extract' function first
if [ -n "$scratch" ]; then
    echo "'scratch' option selected: completely removing previous installation..."
    extract
else
    if [ -d "$QCUSTOMPLOT_ROOT" ]; then
        # In this case we do not extract the tarballs
        echo "Previous installation found. Rebuilding only..."
        # Remove only the build directories
        _clean
    else
        echo "No previous installation found. Starting from scratch..."
        extract
    fi
fi

cd "$QCUSTOMPLOT_LIBS"
# Check that we actually moved
if [ $? -ne 0 ]; then
    exit 1
fi
qmake-qt5
make -j"$N_CORES_USED"

echo "Done."

#Leave the user in the initial directory
cd "$WORKDIR"
