#!/bin/bash

#Functions:

_clean()
{
    echo "Cleaning the build dir(s)..."
    [ -d "$QT_INSTALL_DIR" ] && rm -rf "$QT_INSTALL_DIR"
    [ -d "$QT_BUILD_DIR" ] && rm -rf "$QT_BUILD_DIR"
}

_erase()
{
    _clean
    echo "Removing previously created directories (if any)..."    
    [ -d "$QT_ROOT" ] && rm -rf "$QT_ROOT"
    [ -d "$QT_SVG_DIR" ] && rm -rf "$QT_SVG_DIR"
    [ -d "$QT_PATCH_DIR" ] && rm -rf "$QT_PATCH_DIR"
}

extract()
{
  # If a directory with the target name already exists we remove it first
  _erase
  # Extract the compressed package
  echo "Extracting the tarball(s)..."
  tar -xf "$GPDEXTROOT"/"$QT_SOURCE_NAME".tar.xz --directory "$GPDEXTROOT"
  tar -xf "$QT_SVG_DIR".tar.xz --directory "$GPDEXTROOT"
  # Change the name of the extracted directory to reflect the version number
  mv "$GPDEXTROOT"/"$QT_SOURCE_NAME" "$QT_ROOT"
  # Here we extract and apply the patch to make things work on MacOS 10.13
  # see issue #15
  echo "Extracting the patch for MacOS 10.13 SDK compatibility..."
  tar -xf "$GPDEXTROOT"/"$QT_PATCH_NAME".tar.gz --directory "$GPDEXTROOT"
  source "$QT_PATCH_DIR"/"$QT_PATCH_NAME".sh
  echo "Removing the patch extracted dir (not needed anymore)..."
  [ -d "$QT_PATCH_DIR" ] && rm -rf "$QT_PATCH_DIR"
}

##################

# Main

WORKDIR=$(pwd)

# Check against multiple argument
if [ $# -gt 1 ]; then
   echo "ERROR: Multiple flags 's', 'c', and 'e' not allowed."
   echo "Exiting..."
   exit 1
fi

# The following loop will read the command line options
# see http://linuxcommand.org/lc3_wss0120.php
scratch=
clean= 
erase=
while [ "$1" != "" ]; do
    case $1 in
        -s | --scratch )        shift
                                scratch=0
                                ;;
        -c | --clean )          shift
                                clean=0
                                ;;
        -e | --erase )          shift
                                erase=0
                                ;;
        * )                     exit 1
    esac
    shift
done

# Check if GPDEXTROOT exists (the syntax comes from here)
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if [ -z ${GPDEXTROOT+x} ]; then
    echo "GPDEXTROOT is unset"
    exit 1
fi

# clean option: remove the build directories
if [ -n "$clean" ]; then
    _clean
    exit 0
fi

# erase option: remove everything
if [ -n "$erase" ]; then
    _erase
    exit 0
fi

echo "Installing Qt libraries..."

# If the scratch option is active or if a previous in installation does not
# already exist we need to call the 'extract' function first
if [ -n "$scratch" ]; then
    echo "'scratch' option selected: completely removing previous installation..."
    extract
else
    if [ -d "$QT_ROOT" ]; then
        # In this case we do not extract the tarballs
        echo "Previous installation found. Rebuilding only..."
        # Remove only the build directories
        _clean
    else
        echo "No previous installation found. Starting from scratch..."
        extract
    fi
fi

mkdir -p $QT_BUILD_DIR
mkdir -p $QT_INSTALL_DIR

# We need the following line to avoid a clash of names with a ROOT environment
# variable, which causes 'make install' to copy the files into the wrong
# directory (what happens is that, by default, if a variables named
# $INSTALL_ROOT exists qt prepends its value to the given installation path)
INSTALL_ROOT=""

echo "Building in: $QT_BUILD_DIR"

cd "$QT_BUILD_DIR"
# Check that we actually moved
if [ $? -ne 0 ]; then
    exit 1
fi

"$QT_ROOT"/configure -prefix "$QT_INSTALL_DIR" -opensource -confirm-license -nomake examples -nomake tests

make -j"$N_CORES_USED"
echo "Installing in: $QT_INSTALL_DIR"
make install
echo "Done."

#Get back to the initial directory
cd "$WORKDIR"

# Now make sure that qmake is named qmake-qt5, creating a soft link if not
QMAKE_TARGET_NAME=qmake-qt5
QMAKE_NAME=$(find "$QT_EXECS" -name "qmake*" -print0)

if [ "$QMAKE_NAME" != "$QMAKE_TARGET_NAME" ]; then
    ln -s "$QMAKE_NAME" "$QT_EXECS"/"$QMAKE_TARGET_NAME"
fi

echo "Building qt-svg..."
cd "$GPDEXTROOT"/"$QT_SVG_NAME"
# Check that we actually moved
if [ $? -ne 0 ]; then
    exit 1
fi
$QMAKE_TARGET_NAME
make -j"$N_CORES_USED"
make install

#Leave the user in the initial directory
cd "$WORKDIR"
