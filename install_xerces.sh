#!/bin/bash

WORKDIR=$(pwd)

# Extract the compressed package
tar -xf "$GPDEXTROOT"/"$XERCES_FULL_NAME".tar.gz --directory "$GPDEXTROOT"

mkdir -p "$XERCES_BUILD_DIR"
mkdir -p "$XERCES_INSTALL_DIR"

cd "$XERCES_BUILD_DIR"
echo "Building in $XERCES_BUILD_DIR"
cmake -DCMAKE_INSTALL_PREFIX="$XERCES_INSTALL_DIR" "$XERCES_ROOT" "$XERCES_ROOT"
make -j"$N_CORES_USED"
echo "Done."
echo "Installing in $XERCES_INSTALL_DIR"
make install
echo "Done."
echo "Cleaning source directory..."
make clean
echo "Done."

#Leave the user in the initial directory
cd "$WORKDIR"
