#!/bin/bash

export GPDEXTROOT="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)" #absolute path

#nproc is not defined on macOS, so a distinction is needed
if [ $(uname) == "Darwin" ]; then
    N_CORES=$(sysctl -n hw.physicalcpu)
else
    N_CORES=$(nproc)
fi

# Compile with all the cores available but one
if [ $((N_CORES > 1)) ]; then
    export N_CORES_USED=$((N_CORES - 1))
else
    export N_CORES_USED=$((N_CORES))
fi

# Boost
export BOOST_VERSION=1_73_0
export BOOST_ROOT="$GPDEXTROOT"/boost_"$BOOST_VERSION"
# The Boost.Build tool will be installed here
export B2_INSTALL_DIR="$BOOST_ROOT"/b2_install
export BOOST_BUILD_DIR="$BOOST_ROOT"/bin.v2
export BOOST_INCLUDE="$BOOST_ROOT"
export BOOST_INSTALL_DIR="$BOOST_ROOT"/stage
export BOOST_LIBS="$BOOST_INSTALL_DIR"/lib

# cfitsio
export CFITSIO_VERSION=3_410
export CFITSIO_FULL_NAME=cfitsio_"$CFITSIO_VERSION"
export CFITSIO_ROOT="$GPDEXTROOT"/"$CFITSIO_FULL_NAME"
export CFITSIO_BUILD_DIR="$CFITSIO_ROOT"/build
export CFITSIO_INCLUDE="$CFITSIO_BUILD_DIR"/include
export CFITSIO_LIBS="$CFITSIO_BUILD_DIR"/lib
export CFITSIO_PATCH_NAME=cfitsio_macos_10.14.6_patch
export CFITSIO_PATCH_DIR="$GPDEXTROOT"/"$CFITSIO_PATCH_NAME"

# Geant
export GEANT_VERSION=4.10.03.p01
export GEANT_FULL_NAME=geant"$GEANT_VERSION"
export GEANT_ROOT="$GPDEXTROOT"/"$GEANT_FULL_NAME"
export GEANT_BUILD_DIR="$GPDEXTROOT"/"$GEANT_FULL_NAME"-build
export GEANT_INSTALL_DIR="$GEANT_BUILD_DIR"/"install"
export GEANT_INCLUDE="$GEANT_INSTALL_DIR"/include/Geant4
# On some systems the library dir is called lib64 instad of lib.
# Since we can't figure out how to know this in advance, we add both the path
# (which is not very elegant, but effective)
export GEANT_LIBS="$GEANT_INSTALL_DIR"/lib64
export GEANT_LIBS="$GEANT_INSTALL_DIR"/lib:$GEANT_LIBS
export GEANT_BIN="$GEANT_INSTALL_DIR"/bin
export G4INSTALL=$GEANT_ROOT
export GEANT_DATA=$GEANT_ROOT/data
export G4LEDATAVER=6.50
export G4ENSDFSTATEVER=2.1
export G4LEDATA=$GEANT_DATA/G4EMLOW$G4LEDATAVER
export G4ENSDFSTATEDATA=$GEANT_DATA/G4ENSDFSTATE$G4ENSDFSTATEVER
export GEANT_PATCH_NAME=gcc11_geant4_patch
export GEANT_PATCH_DIR="$GPDEXTROOT"/"$GEANT_PATCH_NAME"

# Qt
export QT_VERSION=5.14.2
export QT_SOURCE_NAME=qtbase-everywhere-src-"$QT_VERSION"
export QT_SVG_NAME=qtsvg-everywhere-src-"$QT_VERSION"
export QT_DIR_NAME=qt"$QT_VERSION"
export QT_ROOT="$GPDEXTROOT"/"$QT_DIR_NAME"
export QT_SVG_DIR="$GPDEXTROOT"/"$QT_SVG_NAME"
export QT_BUILD_DIR="$QT_ROOT"-build
export QT_INSTALL_DIR="$QT_ROOT"-install
export QT_INCLUDE="$QT_INSTALL_DIR"/include
export QT_LIBS="$QT_INSTALL_DIR"/lib
export QT_EXECS="$QT_INSTALL_DIR"/bin
export QT_PATCH_NAME=qt_macos_10.13_patch
export QT_PATCH_DIR="$GPDEXTROOT"/"$QT_PATCH_NAME"

# Add QT_INSTALL_DIR to the path (so that qmake will default to this version)
export PATH="$QT_EXECS":$PATH

# Version of QCustomPlot
export QCUSTOMPLOT_VERSION=1_32
export QCUSTOMPLOT_FULL_NAME=qcustomplot_"$QCUSTOMPLOT_VERSION"
export QCUSTOMPLOT_ROOT="$GPDEXTROOT"/"$QCUSTOMPLOT_FULL_NAME"
export QCUSTOMPLOT_INCLUDE="$QCUSTOMPLOT_ROOT"
export QCUSTOMPLOT_LIBS="$QCUSTOMPLOT_ROOT"/qcustomplot-sharedlib/sharedlib-compilation

#Xerces
export XERCES_VERSION=3.2.0
export XERCES_FULL_NAME=xerces-c-"$XERCES_VERSION"
export XERCES_ROOT="$GPDEXTROOT"/"$XERCES_FULL_NAME"
export XERCES_BUILD_DIR="$GPDEXTROOT"/"$XERCES_FULL_NAME"-build
export XERCES_INSTALL_DIR="$GPDEXTROOT"/"$XERCES_FULL_NAME"-install
export XERCES_INCLUDE="$XERCES_INSTALL_DIR"/include
export XERCES_LIBS="$XERCES_INSTALL_DIR"/lib
